/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadoraaritmetica.java;

import java.util.Scanner;

/**
 *
 * @author Kayck
 */
public class CalculadoraAritmeticaJava {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Scanner MeuTeclado = new Scanner(System.in);
        
        System.out.println("Escolha a operação desejada: ");
        
        System.out.println("1 - Adição ");
        System.out.println("2 - Subtração");
        System.out.println("3 - Divisão");
        System.out.println("4 - Multiplicação");
        
        int operacao = MeuTeclado.nextInt();
        
        switch (operacao){
            case 1:
                System.out.println("Digite o 1° numero");
                int num1 = MeuTeclado.nextInt();
                
                System.out.println("Digite o 2° numero");
                int num2 = MeuTeclado.nextInt();
                
                System.out.println("O Resultado da adição é: " + (num1 + num2));
                break;
                
            case 2:              
                System.out.println("Digite o 1° numero");
                num1 = MeuTeclado.nextInt();
                
                System.out.println("Digite o 2° numero");
                num2 = MeuTeclado.nextInt();
                System.out.println("O Resultado da subtração é: " + (num1 - num2));
                break;
                
            case 3:              
                System.out.println("Digite o 1° numero");
                num1 = MeuTeclado.nextInt();
                
                System.out.println("Digite o 2° numero");
                num2 = MeuTeclado.nextInt();
                
                if (num2 == 0){
                    System.out.println("Divisão por 0 é invalida");
                }
                else{
                    System.out.println("O Resultado da divisão é: " + (num1 / num2));   
                
            }   break;
            
            case 4:
                System.out.println("Digite o 1° numero");
                num1 = MeuTeclado.nextInt();
                
                System.out.println("Digite o 2° numero");
                num2 = MeuTeclado.nextInt();
                
                System.out.println("O Resultado da multiplicação é: " + (num1 * num2));                
            }   break;   
            
            
      
        MeuTeclado.close();
    }
    
}
